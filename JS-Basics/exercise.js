function startExercise(exerciseName) {
    console.log(`\n \n Exercise ${ exerciseName }`);
}

// 1. Exercise
startExercise(1);

let number = 41;
console.log(number);

// 2. Exercise
startExercise(2);

let firstName = "David";
let secondNumber = 42;
let decimal = 12.345;
let boolean = true;
let nullValue = null;
let array = ["Guten Tag", 43, 123.456, false];

const constant = 0.6243299885;

console.log(`Vorname: ${ firstName }`);
console.log(`Zweite Zahl: ${ secondNumber }`);
console.log(`Rationale Zahl: ${ decimal }`);
console.log(`Boolean: ${ boolean }`);
console.log(`Null: ${ nullValue }`);
console.log(`Index 2: ${ array[2] }`);
console.log(`Index 3: ${ array[3] }`);

console.log(array);

// 3. Exercise
startExercise(3);

number = 44;
console.log(number);

// 4. Exercise
startExercise(4);

console.log(number + secondNumber);
console.log(secondNumber + 5);
console.log(secondNumber - number);
console.log(5 - 9);
console.log(`${ array[0] } ${ firstName }!`);
console.log(`Meine erste Ganzzahl ist ${ number }`);

// 5. Exercise
startExercise(5);

let m = array[2] * array[1];
let m1 = array[2] / m;
let r = secondNumber % 4;
let r1 = 10 % 4.5;

// 6. Exercise
startExercise(6);

console.log(`PI: ${ Math.PI }`);

// 7. Exercise
startExercise(7);

console.log(array);

array.push(true);
console.log(array);

array.splice(2, 1);
console.log(array);

array.push(43);
console.log(array);

array.splice(3, 0, "hallo", "welt");
console.log(array);

console.log(`Länge des Arrays: ${ array.length }`);

// 8. Exercise
startExercise(8);

if (array[array.length - 1] === 43) {
    console.log("Das letzte Element im Array ist die Zahl 43");
}

if (true == false) {
    console.log("true ist gleich false");
}
else {
    console.log("true ist ungleich false");
}

if (array[array.length - 1] === 43) {
    console.log("letzte Zahl 43");
}
else if (array[array.length - 1] === 44) {
    console.log("letzte Zahl ist 44");
}

console.log("Die letzte Zahl ist:");
switch(array[array.length - 1]) {
    case 42:
        console.log(42);
        break;
    case 43:
        console.log(43);
        break;
    case 44:
        console.log(44);
        break;
    default:   
        console.log("default");             
}

